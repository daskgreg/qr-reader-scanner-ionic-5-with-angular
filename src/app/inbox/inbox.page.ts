import { Component, OnInit } from '@angular/core';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { NavController, ToastController } from '@ionic/angular';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.page.html',
  styleUrls: ['./inbox.page.scss'],
})
export class InboxPage   {
  bookingID = '0001';
  qrData :any;
  scannedCode = null;
  elementType: 'url' | 'canvas' | 'img' = 'canvas';
  backendAPIDATA:any;
  constructor
  (
     private barcodeScanner: BarcodeScanner,
     private base64ToGallery: Base64ToGallery,
     private toastCtrl:ToastController,
     private navCtrl: NavController,
     private emailComposer: EmailComposer,
  ) 
  { 

  }
  todo = {

  }
  logForm() {
    console.log(this.todo)

    this.todo = JSON.stringify(this.todo);
    console.log(this.todo);
    this.qrData = this.todo;
    this.backendAPIDATA = this.todo;
  }


  scanCode() 
  {
    this.barcodeScanner.scan().then
    (
      barcodeData => 
      {
        this.scannedCode = barcodeData;
      }
    );
  }
  
  downloadQR(){
    const canvas = document.querySelector('canvas') as HTMLCanvasElement;
    const imageData = canvas.toDataURL('image/jpeg').toString();
    console.log('data: ',imageData);

    // Splitting the string of the image at the fist ,  
    let data = imageData.split(',')[1];

    // let data is the string that the backend-api will receive with 
    //  qrData :any;
    //  qrData is an array which is generating a barcode with specific values eg firstName: greg,lastName dask
    // in that way we make sure that every barcode is going to be unique 

    console.log("QR data:",data);
    
    // For Cordova Platforms Like Android and ios 
    this.base64ToGallery.base64ToGallery
    (data,
       { prefix: '_qrCodeImg', mediaScanner: true }
    ).then
    ( 
      async res => 
      {
        let toast = await this.toastCtrl.create({
          header:'QR code saved'
        });
        toast.present();
      },err => console.log('err:', err));
    }
    
  currentImage = null;

 
  // sendEmail(){
  //   let email = {
  //     to: 'dask.gregory@gmail.com',
  //     cc: 'gregdaskalakhs@gmail.com',
  //     attachments: [
  //       this.currentImage
  //     ],
  //     subject: 'BOOKING ID:' + this.bookingID + 'Bus Passenger',
  //     body: 'Thank you very much for your reservation'
  //          + '<br><br>' 
  //          + 'First Name: ' + this.todo.firstname 
  //          + 'Last Name: ' + this.todo.lastname 
  //          + 'Phone Number ' + this.todo.firstname 
  //          + 'Date Of Birth: ' + this.todo.firstname 
  //          + 'Booking ID: ' + this.bookingID,
  //     isHtml: true
  //   };
  //   this.emailComposer.open(email);
  //   console.log('email sended successfully');
  // }

}
