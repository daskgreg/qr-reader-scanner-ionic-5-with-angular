import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'inbox',
    pathMatch: 'full'
  },
  {
      path: 'inbox',
      loadChildren: () => import('./inbox/inbox.module').then( m => m.InboxPageModule)
     
  },
  {
    path: 'qr-reader',
    loadChildren: () => import('./qr-reader/qr-reader.module').then( m => m.QrReaderPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
